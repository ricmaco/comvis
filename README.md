Comunicazione Visiva e Multimodale 2014-2015
============================================

Autori
------

- Sara Cifola - <sara.cifola@studenti.unimi.it>
- Diana Lazzarin - <diana.lazzarin@studenti.unimi.it>
- Ludovica Cannas - <ludovica.cannas@studenti.unimi.it>
- Leonardo Lombardi - <leonardo.lombardi@studenti.unimi.it>
- Riccardo Macoratti - <riccardo.macoratti@studenti.unimi.it>

Consegna
--------

Modellare una nuova interfaccia per un sito di un'**organizzazione no profit** secondo i principi del design delle interfacce.

Soggetto
--------

Si è scelto il sito dell'associazione [**VAMIO** *Volontari Assistenza Minori In Ospedale*](http://www.vamio.org/).
