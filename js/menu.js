/**
 * Ritorna true se page è la pagina corrente.
 * False altrimenti.
 */
function isCurrentPage(page) {
  var current = document.location.pathname.split("/");
  current = current[current.length-1];
  return current == page;
}

/**
 * Controlla se il link appartiene a un meno di
 * secondo livello. In caso positivo mette in
 * grassetto anche il parent.
 */
function findParent(son, menu) {
  var parent;
  for (var j=son-1; j>=0; --j) {
    var href = menu[j].href.split("/");
    href = href[href.length-1];
    if (href.indexOf("#") > -1) {
      parent = j;
      break;
    }
  }
  if (parent != null) {
    menu[parent].className += " " + "menu-current";
  }
}

/**
 * Mette in grassetto la pagina corrente nel menu.
 */
function setActiveMenu() {
  var menu = document.getElementById("nav").getElementsByTagName("a");
  for (var i=0; i<menu.length; ++i) {
    var href = menu[i].href.split("/");
    href = href[href.length-1];
    if (isCurrentPage(href)) {
      menu[i].className += " " + "menu-current";
      findParent(i, menu);
    }
  }
}

/**
 * Aggiunge il controllo della pagina corrente nel menu, sotto forma
 * di listener di eventi, senza sovrascrivere dichiarazioni
 * precedenti.
 */
window.addEventListener("load", setActiveMenu);
