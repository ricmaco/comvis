/**
 * Aggiunge il contenuto dell'anno scelto al div predisposto.
 * Applica la classe per segnalare l'anno corrente al anchor corretto.
 * TODO: togliere la classe current al'anno selezionato precedentemente.
 */
function getYearContent(year) {
  var content = document.getElementById("content-" + year).innerHTML;
  document.getElementById("years-activities").innerHTML = content;
  
  var selector = document.getElementById("selector-activities").getElementsByTagName("a");
  for (var i=0; i<selector.length; ++i) {
    if (selector[i].className == "selector-current") {
      selector[i].className = "";
    }
    if (selector[i].innerHTML == year) {
      selector[i].className = "selector-current";
    }
  }
}
